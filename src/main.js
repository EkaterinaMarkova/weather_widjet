import { createApp } from 'vue'
import App from './App.vue'
import { Quasar } from 'quasar'
import quasarUserOptions from './quasar-user-options'
import axios from 'axios'

const app = createApp(App)
app.use(Quasar, quasarUserOptions)

const api = axios.create({ baseURL: 'https://api.openweathermap.org' })
const API_KEY = 'b98f155b75f532c57d6ca592ac44feb2'
app.config.globalProperties.$api = api;
app.config.globalProperties.$key = API_KEY

app.mount('#app')


