### Weather widget

**Stack**
1. Vue3 - https://v3.ru.vuejs.org/
2. Axios - https://axios-http.com/ru/docs/intro
3. Quasar - https://quasar.dev/

**Function:**
1. Main card - show weather of selected locations

      ![Main card](public/img/widget.png)

2. Settings:
    - Select locations. You can add up to 5 locations
    
     ![Select locations](public/img/imgSettingsSelect.png)

    - Remove and reorder locations (use icon)
    
    ![Function](public/img/imgSettings.png)


If you see such a card, then the data is being loaded

![Wait card](public/img/imgWait.png)



**By default** widget requests geodata. If the user has blocked the receipt of the location, then Moscow will be selected at the first start.

**❌️Failed requirement:❌️**

1. Users should be able to add this widget to their websites as simple as inserting the snippet into an HTML-page:

        `<weather-widget /> <script type="text/javascript" src="{URL to the app}"></script>`
        
After searching, I came to the conclusion that I need to use custom element. This is a new concept for me, I need more time to figure it out. First of all, import dependencies, which means writing a wrapper function for build.

**Some problems:**

There is an assumption that the reorder does not always work correctly. So far I can't find the bug.

**Improvements:**

1. Add store(VueX or Pinia) and move work with data. Thanks to this, data loading and updating will be accelerated, the load on the api will decrease, and there will be a separation into views and logic.
2. Add dark theme
3. Add collapse location's cards.For example, show only temperature + icon and location
4. By default Moscow location is used. Try to calculate by time zone default location